function addNumbers(operandOne, operandTwo){
    var sumOfInputs  =  0;
    var errorMessage =  {"message" : "The inputs should be valid numbers or convertible strings. Please fix & try again."}
    
    var validationResponse = validateInputs(operandOne, operandTwo);
    if(!validationResponse){
        return errorMessage;
    }
    // Business logic
    sumOfInputs = parseFloat(operandOne) + parseFloat(operandTwo);
    return sumOfInputs;
}

function validateInputs(operandOne, operandTwo){
    // small & has one responsibility

    var validationResult = true;
    // validation logic
    /* validation:
        - the inputs should be numbers (make sure that the inputs are not strings).
        - The strings that are convertible into numbers should be accepted
    */
    // Fix the logic
    if(typeof operandOne == "string" || typeof operandTwo == "string"){
    if( (parseFloat(operandOne) == NaN || parseFloat(operandTwo) == NaN) ){
        return false;
    }
}
    return validationResult;
}

console.log(addNumbers(10,20.60));
console.log(addNumbers(20,"50"));
console.log(addNumbers("20", 50));
console.log(addNumbers(20, 50));
console.log(addNumbers("20.90", 50.80));
console.log(addNumbers("C", "D"));
console.log(addNumbers("2CD", "CD"));
console.log(addNumbers("2CD", "34CD"));

//VM143:32 30.6
//VM143:33 70
//VM143:34 70
//VM143:35 70
//VM143:36 71.69999999999999
//VM143:37 NaN
//VM143:38 NaN
VM143:39 36//