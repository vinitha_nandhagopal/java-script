* Explain closure. Write an example using a cell phone. */
/* Most of the JavaScript Developers use closure consciously or unconsciously. Even if they do unconsciously
 //it works fine in most of the cases. But knowing closure will provide better control over the code when using them. */

  function CellPhone(redmi, oneplus, realme, oppo ) {
    return {
      toString() {
        return "${redmi} ${oneplus} (${real me}, ${oppo})"
      }
    }
  }
  const cellPhone = new CellPhone('apple','iphone','2019','black and white')
  console.log(cellPhone.toString())
