function addNumbers(operandOne, operandTwo){ 
    /* 
        Goal: Add two numbers & return a single value or returns a JSON as response.
        Example response: { "message": "Invalid Inputs. This function accepts only integers." }
        Example valid addition (float or integer based on the input): 40.50 or 40 
    */
    var resultOfAddition    = 0;
        // 1. validate inputs
                var validationResponse = validateInputs(operandOne, operandTwo);
                if(!validationResponse){
                    return { "message": "Invalid Inputs. This function accepts only integers." }
                }        
        // 2. implement business logic
                resultOfAddition    = parseFloat(operandOne) + parseFloat(operandTwo)
        // 3. respond (return a value)
                return resultOfAddition;        
    
}

function validateInputs(operandOne, operandTwo){
        // The inputs should be integers (10, 20) or doubles (10.5, 20.6).
        // Edge scenario: "10.5", "20". In this case, we should "typecast" (converting one type to another type)
            if(typeof operandOne == "string" || typeof operandTwo == "string"){
                if(parseFloat(operandOne) == NaN || parseFloat(operandTwo) == NaN){
                    return false;
                }
            }
            return true;
}
console.log(addNumbers(10,20));
