function addSquares(a, b) {
  function square(x) {
    return x * x;
  }
  return square(a) + square(b);
}
a = addSquares(2, 3); // returns 13
b = addSquares(3, 4); // returns 25
c = addSquares(4, 5);
41
addSquares
ƒ addSquares(a, b) {
  function square(x) {
    return x * x;
  }
  return square(a) + square(b);
}
//a
//13
//b
//25
//c
//41
//(a,b)
//25
//(a,c)
//41
//(a+b+c)
//79
//(a+b)
38