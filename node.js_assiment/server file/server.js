var express     = require('express');

// create server
var server      = express();

// import path
var path        = require('path');

// use 'public' folder that holds the static files
var public      = path.join(__dirname, 'public/html');

// map the public folder in middleware
server.use(express.static(public));

// serve(render) the file on route
server.get('/course', function(request, response) {
    response.sendFile(path.join(public, 'course.html'));
});

server.get('/practicals', function(request, response) {
    response.sendFile(path.join(public, 'practicals.html'));
});

server.get('/image', function(request, response) {
        response.sendFile(path.join(public, 'img.html'));
});

server.listen(3000, function(){
    console.log("Server started successfully.")
});
