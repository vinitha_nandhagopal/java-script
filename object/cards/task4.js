console.log("4) Can you think of a way to determine which card appears the most frequently?")
			answer = [...cardMap.entries()].reduce((returnValue, next) => {return returnValue[1] > next[1] ? returnValue : next});
			console.log(`\t ${answer[0]}, It appears ${answer[1]} times`);
// 4) Can you think of a way to determine which card appears the most frequently?
// 	 spades_nine, It appears 116 times