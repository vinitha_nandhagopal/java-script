console.log(`6) What is the average number of followers among those users associated with tweets that contain
"lol" (case insensitive)?`);
			let lolArray = tweets.filter(tweet => {
				return tweet.text.includes("lol");
			});	
			let total_followers_count = lolArray.reduce((returnValue, next) => {return returnValue + next.user.followers_count}, 0);
			console.log(`\t Answer = ${total_followers_count/lolArray.length} `);
			
            // What is the average number of followers among those users associated with tweets that contain
//"lol" (case insensitive)?
// 	 Answer = 776.2307692307693